#!/bin/sh

echo $1
cp envlog.txt results/default/
cp memory1 results/default/
cp memory_recurrent results/default/
cp datafile.txt results/default/
cp datafig_big.png results/default/
cp -r results/default results/$1
