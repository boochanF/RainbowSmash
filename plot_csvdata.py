# %%
import os
import time
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

col_names = ["t-steps", "reward", "length", "Q", "loss", "l-rate", "std_weights"]
# df = pd.read_csv("datafile.txt", names=col_names)

# f = df.plot(x="t-steps", y="Q")
# f = df.plot(x="t-steps", y="loss")
# f = df.plot(y="reward")

# %% this is for streaming
def plot_stream_fig():
    goalval = 0.5
    f, a = plt.subplots(2, 1, figsize=(6, 7))
    df = pd.read_csv("datafile.txt", names=col_names)
    dlen = len(df["reward"])
    a[0].plot(df["reward"], linewidth=2)
    # a[0].plot([0, dlen], [goalval, goalval], linewidth=2)
    fn = "Comic Sans MS"
    a[0].set_ylabel("Ave. Reward", fontsize=20, fontname=fn)
    plt.axes(a[0])
    plt.xticks(fontsize=15, fontname=fn)
    plt.yticks(fontsize=15, fontname=fn)
    bgcolor_a = (1.0, 1.0, 1.0, 0.0)
    bgcolor_f = (1.0, 1.0, 1.0, 0.3)
    a[0].set_facecolor(bgcolor_a)
    f.patch.set_facecolor(bgcolor_f)
    plt.box("off")
    a[0].spines["right"].set_visible(False)
    a[0].spines["top"].set_visible(False)
    # a[0].annotate("Goal", (dlen + 1, goalval - 0.1), fontsize=15, fontname=fn)

    plt.axes(a[1])
    a[1].plot(df["Q"], linewidth=2)
    a[1].set_ylabel("Ave. Q", fontsize=20, fontname=fn)
    plt.xticks(fontsize=15, fontname=fn)
    plt.yticks(fontsize=15, fontname=fn)
    a[1].set_facecolor(bgcolor_a)
    plt.box("off")
    a[1].spines["right"].set_visible(False)
    a[1].spines["top"].set_visible(False)
    a[1].set_xlabel("Episode number", fontsize=20, fontname=fn)

    plt.style.use("default")

    plt.tight_layout()
    plt.savefig("datafig.png")
    f.clear()
    plt.close(f)


# %% this is for my reference


def plot_stuff(data, ax, color):
    dlen = len(data)
    ax.plot(data, linewidth=2, color=color)
    # ax.plot([0, dlen], [1.5, 1.5], linewidth=5)
    fn = "Helvetica"
    ax.set_xlabel("Episode number", fontsize=10, fontname=fn)
    plt.xticks(fontsize=10, fontname=fn)
    # plt.yticks(fontsize=10, fontname=fn, color=color)
    plt.yticks(fontsize=10, fontname=fn)
    # bgcolor_a = (1.0, 1.0, 1.0, 0.0)
    # bgcolor_f = (1.0, 1.0, 1.0, 0.3)
    # ax.set_facecolor(bgcolor_a)
    # fig.patch.set_facecolor(bgcolor_f)
    plt.box("off")
    # ax.spines["right"].set_visible(False)
    # ax.spines["top"].set_visible(False)


def plot_all(df):
    f, a = plt.subplots(2, 1, figsize=(10, 10))
    c1 = "tab:blue"
    c2 = "tab:orange"
    a20 = a[0].twinx()
    a21 = a[1].twinx()
    plot_stuff(df["reward"], a20, c1)
    plot_stuff(df["length"], a[0], c2)
    plot_stuff(df["Q"], a21, c1)
    plot_stuff(df["loss"], a[1], c2)
    a20.set_ylabel("Reward", color=c1)
    a[0].set_ylabel("Episode Length", color=c2)
    a21.set_ylabel("Q", color=c1)
    a[1].set_ylabel("Loss", color=c2)
    plt.style.use("default")
    plt.tight_layout()
    plt.savefig("datafig_big.png")
    time.sleep(1)
    f.clear()
    plt.close(f)


dfname = "datafile.txt"
prev_time = 0

# df = pd.read_csv(dfname, names=col_names)
# plt.figure()
# plt.plot(df["loss"])
# a2 = plt.gca().twinx()
# a2.plot(df["l-rate"], "r")
# plt.show()
# plt.figure()
# plt.plot(np.array(df["loss"]), np.array(df["reward"]))
# plt.figure()
# plt.scatter(np.array(df["l-rate"]), np.array(df["Q"]))
# plt.figure()
# plt.scatter(np.array(df["l-rate"]), np.array(df["length"]))
# plt.show()

while True:
    current_time = os.path.getmtime(dfname)
    if current_time > prev_time:
        df = pd.read_csv(dfname, names=col_names)
        plot_all(df)
        plot_stream_fig()
        prev_time = current_time
    time.sleep(60)
