# simple memory definition

import numpy as np
import torch
import torch.nn.functional as F
from collections import deque

Transition_dtype = np.dtype(
    [
        ("timestep", np.int32),
        ("state", np.uint8, (84, 84)),
        ("action", np.int32),
        ("reward", np.float32),
        ("nonterminal", np.bool_),
    ]
)
blank_trans = (0, np.zeros((84, 84), dtype=np.uint8), 0, 0.0, False)


class SimpleMemory:
    def __init__(self, args, capacity, action_space):
        self.action_space = action_space
        self.device = args.device
        self.capacity = capacity
        self.history = args.history_length
        self.discount = args.discount

        self.priority_weight = args.priority_weight
        self.priority_exponent = args.priority_exponent

        self.n = args.multi_step
        # Discount-scaling vector for n-step returns
        self.n_step_scaling = np.flip([self.discount ** i for i in range(self.n)])

        # self.games = deque([], maxlen=int(self.capacity / 1200))
        self.games = []
        self.total_steps = 0
        self.memory_capacity = args.memory_capacity
        self.append_pool = []
        self.current_game = None  # do not set the first one...
        self.current_time = 1

    def append(self, game):
        # g = np.empty(len(game), dtype=object)
        nsteps = len(game)
        g = np.array([blank_trans] * nsteps, dtype=Transition_dtype)
        for i, s in enumerate(game):
            state = s[0][0] * 255
            g[i] = (i, state, s[1], s[2], not s[3])

        # appended to temporary pool not to disrupt self.games
        self.append_pool.append(g)

    def sample(self, batch_size):
        # if it's in a current game, continue providing it.
        # otherwise, choose another game

        if self.current_game is None:
            # first time call.
            data = None
            bs = None
        else:
            data, bs = self.get_batch(batch_size)

        while data is None:
            if len(self.append_pool) > 0:
                # transfer games from pool to games
                while len(self.append_pool) > 0:
                    g = self.append_pool.pop(0)
                    self.total_steps += len(g)
                    self.games.append(g)
                # remove first games until total steps goes below limit
                while self.total_steps >= self.memory_capacity:
                    self.total_steps -= len(self.games.pop(0))

                self.current_game = len(self.games) - 1  # semi-on-line training
                # self.appended = False
            else:
                self.current_game = np.random.randint(len(self.games))
            self.current_time = 1
            data, bs = self.get_batch(batch_size)

        self.current_time += bs
        return data

    def get_batch(self, batch_size):
        game = self.games[self.current_game]
        t = self.current_time

        if len(game) <= (t + self.n):
            return None, batch_size
        if len(game) <= (t + self.n + batch_size):
            bs = len(game) - (t + self.n)  # shrink the batch size
        else:
            bs = batch_size

        state = torch.Tensor(game["state"][t : t + bs]).div(255).unsqueeze(1)
        next_action = torch.LongTensor(game["action"][t : t + bs].copy())
        action = F.one_hot(
            torch.LongTensor(game["action"][t - 1 : t - 1 + bs].copy()),
            num_classes=self.action_space,
        ).float()

        n_state = (
            torch.Tensor(game["state"][t + self.n : t + self.n + bs])
            .div(255)
            .unsqueeze(1)
        )

        n_action = F.one_hot(
            torch.LongTensor(
                game["action"][t - 1 + self.n : t - 1 + self.n + bs].copy()
            ),
            num_classes=self.action_space,
        ).float()

        rewards = game["reward"][t : t + self.n + bs - 1]
        return_ = torch.Tensor(
            np.convolve(rewards, self.n_step_scaling, "valid"),
        ).float()

        nonterminals = game["nonterminal"][t + self.n : t + self.n + bs]
        nonterminals = torch.tensor(nonterminals).unsqueeze(1)

        return (
            None,
            (state, action),
            next_action,
            return_,
            (n_state, n_action),
            nonterminals,
            torch.Tensor([1]),
        ), bs

    def update_priorities(*args):
        # fake function...
        return
