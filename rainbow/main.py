# -*- coding: utf-8 -*-
from __future__ import division
import argparse
import bz2
from datetime import datetime
import os
import pickle
import cv2
from collections import deque

import atari_py
import numpy as np
import torch
from tqdm import trange

from agent import Agent

from memory import ReplayMemory
from simple_memory import SimpleMemory

import threading
from math import cos, pi
import time

import pandas as pd

# Note that hyperparameters may originally be reported in ATARI game frames instead of agent steps
parser = argparse.ArgumentParser(description="Rainbow")
parser.add_argument("--id", type=str, default="default", help="Experiment ID")
parser.add_argument("--seed", type=int, default=123, help="Random seed")
parser.add_argument("--disable-cuda", action="store_true", help="Disable CUDA")
parser.add_argument(
    "--game",
    type=str,
    default="smash_bros",
    choices=atari_py.list_games(),
    help="ATARI game",
)
parser.add_argument(
    "--T-max",
    type=int,
    default=int(50e6),
    metavar="STEPS",
    help="Number of training steps (4x number of frames)",
)
parser.add_argument(
    "--max-episode-length",
    type=int,
    default=int(108e3),
    metavar="LENGTH",
    help="Max episode length in game frames (0 to disable)",
)
parser.add_argument(
    "--history-length",
    type=int,
    default=4,
    metavar="T",
    help="Number of consecutive states processed",
)
parser.add_argument(
    "--architecture",
    type=str,
    default="canonical",
    choices=["canonical", "data-efficient", "recurrent"],
    metavar="ARCH",
    help="Network architecture",
)
parser.add_argument(
    "--hidden-size", type=int, default=512, metavar="SIZE", help="Network hidden size"
)
parser.add_argument(
    "--lstm-layers", type=int, default=1, metavar="LAYERS", help="Number of LSTM layers"
)
parser.add_argument(
    "--noisy-std",
    type=float,
    default=0.1,
    metavar="σ",
    help="Initial standard deviation of noisy linear layers",
)
parser.add_argument(
    "--atoms",
    type=int,
    default=51,
    metavar="C",
    help="Discretised size of value distribution",
)
parser.add_argument(
    "--V-min",
    type=float,
    default=-10,
    metavar="V",
    help="Minimum of value distribution support",
)
parser.add_argument(
    "--V-max",
    type=float,
    default=10,
    metavar="V",
    help="Maximum of value distribution support",
)
parser.add_argument(
    "--model", type=str, metavar="PARAMS", help="Pretrained model (state dict)"
)
parser.add_argument(
    "--memory-capacity",
    type=int,
    default=int(1e6),
    metavar="CAPACITY",
    help="Experience replay memory capacity",
)
parser.add_argument(
    "--replay-frequency",
    type=int,
    default=4,
    metavar="k",
    help="Frequency of sampling from memory",
)
parser.add_argument(
    "--priority-exponent",
    type=float,
    default=0.5,
    metavar="ω",
    help="Prioritised experience replay exponent (originally denoted α)",
)
parser.add_argument(
    "--priority-weight",
    type=float,
    default=0.4,
    metavar="β",
    help="Initial prioritised experience replay importance sampling weight",
)
parser.add_argument(
    "--multi-step",
    type=int,
    default=3,
    metavar="n",
    help="Number of steps for multi-step return",
)
parser.add_argument(
    "--discount", type=float, default=0.99, metavar="γ", help="Discount factor"
)
parser.add_argument(
    "--target-update",
    type=int,
    default=int(8e3),
    metavar="τ",
    help="Number of steps after which to update target network",
)
parser.add_argument(
    "--reward-clip",
    type=int,
    default=0,
    metavar="VALUE",
    help="Reward clipping (0 to disable)",
)
parser.add_argument(
    "--learning-rate", type=float, default=0.0000625, metavar="η", help="Learning rate"
)
parser.add_argument(
    "--adam-eps", type=float, default=1.5e-4, metavar="ε", help="Adam epsilon"
)
parser.add_argument(
    "--batch-size", type=int, default=32, metavar="SIZE", help="Batch size"
)
parser.add_argument(
    "--norm-clip",
    type=float,
    default=10,
    metavar="NORM",
    help="Max L2 norm for gradient clipping",
)
parser.add_argument(
    "--learn-start",
    type=int,
    default=int(20e3),
    metavar="STEPS",
    help="Number of steps before starting training",
)
parser.add_argument("--evaluate", action="store_true", help="Evaluate only")
parser.add_argument(
    "--evaluation-interval",
    type=int,
    default=100000,
    metavar="STEPS",
    help="Number of training steps between evaluations",
)
parser.add_argument(
    "--evaluation-episodes",
    type=int,
    default=10,
    metavar="N",
    help="Number of evaluation episodes to average over",
)
# TODO: Note that DeepMind's evaluation method is running the latest agent for 500K frames ever every 1M steps
parser.add_argument(
    "--evaluation-size",
    type=int,
    default=500,
    metavar="N",
    help="Number of transitions to use for validating Q",
)
parser.add_argument(
    "--render", action="store_true", help="Display screen (testing only)"
)
parser.add_argument(
    "--enable-cudnn",
    action="store_true",
    help="Enable cuDNN (faster but nondeterministic)",
)
parser.add_argument(
    "--checkpoint-interval",
    default=0,
    help="How often to checkpoint the model, defaults to 0 (never checkpoint)",
)
parser.add_argument("--memory", help="Path to save/load the memory from")
parser.add_argument(
    "--disable-bzip-memory",
    action="store_true",
    help="Don't zip the memory file. Not recommended (zipping is a bit slower and much, much smaller)",
)
parser.add_argument(
    "--quick-mode", action="store_true", help="Enable quick (1.5x) mode (30 fps)"
)
parser.add_argument(
    "--input-latency",
    type=int,
    default=6,
    metavar="LATENCY",
    help="Input latency value used for rewinding the reward timing (only for Smash bros.)",
)
parser.add_argument(
    "--weight-decay",
    type=float,
    default=0.0,
    metavar="DECAY",
    help="Weight decay for AdamW. Original Rainbow does not have it",
)
parser.add_argument(
    "--num-stocks",
    type=int,
    default=3,
    metavar="STOCKS",
    help="Number of stocks for smash bros.",
)
parser.add_argument(
    "--suicide-punishment",
    type=float,
    default=0.0,
    metavar="SUICIDE",
    help="Extra punishment for suicide death.",
)

# Setup
args = parser.parse_args()
args.checkpoint_interval = int(args.checkpoint_interval)

print(" " * 26 + "Options")
for k, v in vars(args).items():
    print(" " * 26 + k + ": " + str(v))
results_dir = os.path.join("results", args.id)
if not os.path.exists(results_dir):
    os.makedirs(results_dir)
metrics = {"steps": [], "rewards": [], "Qs": [], "best_avg_reward": -float("inf")}
np.random.seed(args.seed)
torch.manual_seed(np.random.randint(1, 10000))
if torch.cuda.is_available() and not args.disable_cuda:
    args.device = torch.device("cuda")
    torch.cuda.manual_seed(np.random.randint(1, 10000))
    torch.backends.cudnn.enabled = args.enable_cudnn
else:
    args.device = torch.device("cpu")


stop_thread = False


class Trainer(threading.Thread):
    def __init__(self, dqn, mem, args, counter, mov_l):
        threading.Thread.__init__(self)
        self.dqn = dqn
        self.mem = mem
        self.args = args
        self.counter = counter
        self.priority_weight_increase = (1 - args.priority_weight) / (
            args.T_max - args.learn_start
        )
        self.alpha = 0.0005
        self.mov_l = mov_l

    def run(self):
        period = 100000
        self.dqn.train()
        global save_memory_flag
        if save_memory_flag:
            logging.info("Saving Memory into a file...")
            save_memory(self.mem, self.args.memory, self.args.disable_bzip_memory)
            self.dqn.save(results_dir)
            save_memory_flag = False
        global stop_thread
        while not stop_thread:
            self.mov_l = self.mov_l * (1 - self.alpha) + self.alpha * self.dqn.mean_loss
            # if self.counter % 10000 == 0:
            #    print("")
            # self.dqn.set_lr(
            #     self.args.learning_rate * 2 ** (-cos(self.counter / period * 2 * pi))
            # )
            self.mem.priority_weight = min(
                self.mem.priority_weight - self.priority_weight_increase, 1
            )
            self.dqn.learn(self.mem)
            self.counter += 1
            if self.counter % self.args.target_update == 0:
                self.dqn.update_target_net()
            if self.counter % self.args.replay_frequency == 0:
                self.dqn.reset_noise()
        # thread stopped
        return


# Simple ISO 8601 timestamped logger
def log(s):
    print("[" + str(datetime.now().strftime("%Y-%m-%dT%H:%M:%S")) + "] " + s)


def load_memory(memory_path, disable_bzip):
    if disable_bzip:
        with open(memory_path, "rb") as pickle_file:
            return pickle.load(pickle_file)
    else:
        with bz2.open(memory_path, "rb") as zipped_pickle_file:
            return pickle.load(zipped_pickle_file)


def save_memory(memory, memory_path, disable_bzip):
    if disable_bzip:
        with open(memory_path, "wb") as pickle_file:
            pickle.dump(memory, pickle_file)
    else:
        with bz2.open(memory_path, "wb") as zipped_pickle_file:
            pickle.dump(memory, zipped_pickle_file)


# Environment
if args.game == "smash_bros":
    from env import Env
    from env import logging
    from test import test
else:
    from env_orig import Env
    import logging

    logging.basicConfig(
        filename="envlog_atari.txt",
        filemode="w",
        level=logging.INFO,
        format="%(asctime)s %(message)s",
    )


# store option in envlog.txt
for k, v in vars(args).items():
    logging.info(k + ": " + str(v))

env = Env(args)
env.reset()
# env.train()
# action_space = env.action_space.n
action_space = env.action_space()

# Agent
dqn = Agent(args, env)
dqn_ac = Agent(args, env)

# memory model
if args.architecture == "recurrent":
    Memory = SimpleMemory
else:
    Memory = ReplayMemory


# If a model is provided, and evaluate is false, presumably we want to resume, so try to load memory
if args.model is not None and not args.evaluate:
    if os.path.exists(args.memory):  # load
        mem = load_memory(args.memory, args.disable_bzip_memory)
    else:  # create a new one
        print("Pre-trained model, starting with fresh memory")
        mem = Memory(args, args.memory_capacity, action_space)
else:
    mem = Memory(args, args.memory_capacity, action_space)

priority_weight_increase = (1 - args.priority_weight) / (args.T_max - args.learn_start)

arrows = ["·", "→", "↘", "↓", "↙", "←", "↖", "↑", "↗"]
buttons = [".", "A", "B", "J", "G", "S"]


def actiontext(action):
    b = action // 9
    s = action % 9
    return arrows[s] + buttons[b]


trainer = Trainer(dqn, mem, args, 0, 2.3)


# Construct validation memory
val_mem = ReplayMemory(args, args.evaluation_size, action_space)
T, done = 0, True
while T < args.evaluation_size:
    if done:
        state = env.reset()

    action = np.random.randint(0, action_space)
    next_state, _, done, _ = env.step(action)
    val_mem.append(state, action, 0.0, done)
    state = next_state
    T += 1


if args.evaluate:
    dqn_ac.eval()  # Set DQN (online network) to evaluation mode
    avg_reward, avg_Q = test(
        args, 0, dqn_ac, val_mem, metrics, results_dir, evaluate=True
    )  # Test
    print("Avg. reward: " + str(avg_reward) + " | Avg. Q: " + str(avg_Q))
    save_memory(val_mem, "val_mem_dump.bin", False)
else:
    # Training loop
    # dqn.train()
    # read data from a file (if that exist)
    if os.path.isfile("datafile.txt"):
        col_names = ["t-steps", "reward", "length", "Q", "loss", "l-rate", "ave_weight"]
        df = pd.read_csv("datafile.txt", names=col_names)
        mov_r = list(df["reward"])[-1]
        mov_q = list(df["Q"])[-1]
        mov_leng = list(df["length"])[-1]
        trainer.mov_l = list(df["loss"])[-1]
    else:
        # some arbitrary default values
        mov_r = 0.0
        mov_q = 0.0
        mov_leng = 600 * args.num_stocks  # average episode length

    dqn_ac.train()
    T, done = 0, True
    tempmem = []
    all_reward = None
    save_memory_flag = False
    env.steps = 0
    epi_reward = 0
    epi_counter = 0
    tr = trange(1, args.T_max + 1)
    alpha = 0.0001
    alpha2 = 0.05
    counter = 1000  # default value
    ts = 0
    Q = 0
    wins_buffer = deque([False] * 100, maxlen=100)
    cleared = False

    for T in tr:
        if done:
            print("")
            stop_thread = True
            if trainer.is_alive():
                trainer.join()
                dqn_ac.online_net.load_state_dict(trainer.dqn.online_net.state_dict())

            if type(trainer.mem) == SimpleMemory:
                if len(tempmem) > 0:  # make sure this is not empty
                    # do a special treatment for reward if it's smash
                    if type(all_reward) is np.ndarray:  # means this is smash
                        for i in range(len(all_reward)):
                            tempmem[i][2] = all_reward[i]

                    trainer.mem.append(tempmem)
            else:
                for m in tempmem:
                    trainer.mem.append(*m)
                if type(all_reward) is np.ndarray:
                    for i in range(len(all_reward)):
                        trainer.mem.transitions.data[
                            trainer.mem.transitions.index - 1 - i
                        ][3] = all_reward[-i]

            # this part could be simpler by just changing tempmem, but not dealing with it now.
            tempmem = []
            stop_thread = False
            if counter > 0:
                rep_freq = (trainer.counter - ts) / counter
            else:
                rep_freq = 0
            ts = trainer.counter
            if T >= args.learn_start:
                trainer = Trainer(
                    trainer.dqn, trainer.mem, args, trainer.counter, trainer.mov_l
                )
                trainer.start()

            lr = trainer.dqn.get_lr()
            ave_weight = trainer.dqn.online_net.get_ave_weight()
            mov_l = trainer.mov_l
            mov_leng = mov_leng * (1 - alpha2) + alpha2 * counter
            mov_r = mov_r * (1 - alpha2) + alpha2 * epi_reward
            wins_buffer.append(epi_reward > 0)  # definition of win
            logging.info(f"episode_reward: {epi_reward:0.2f}")
            logging.info(f"reward moving average: {mov_r:0.2f}")
            logging.info(f"cat's confidence: {(2.4-trainer.mov_l)*1000:0.1f}")

            epi_counter += 1
            epi_reward = 0
            counter = 0
            with open("winrate.txt", "w") as f:
                win_rate = int(sum(wins_buffer))
                if win_rate >= 70:
                    cleared = True
                if cleared:
                    magic = "Goal!"
                else:
                    magic = 0
                    remaining = 70 - win_rate
                    for i in range(len(wins_buffer)):
                        magic += 1
                        remaining -= int(wins_buffer[i] == False)
                        if remaining == 0:
                            break
                f.write(
                    f"{int(sum(wins_buffer) * 100 / min(100, epi_counter)):2d} {magic}"
                )

            with open("datafile.txt", "a") as f:
                f.write(f"{ts},{mov_r},{mov_leng},{mov_q},{mov_l},{lr},{ave_weight}\n")
            if args.architecture == "recurrent":
                dqn_ac.online_net.reset_states()
            state = env.reset()

        # from here, minimize interacting with trainer.
        if T % args.replay_frequency == 0:
            dqn_ac.reset_noise()  # Draw a new set of noisy weights
        # Choose an action greedily (with noisy weights)
        if T >= args.learn_start:
            action, Q = dqn_ac.act(state)
        else:  # select random action
            if args.model is not None:
                action, Q = dqn_ac.act(state)
            else:
                action = np.random.randint(action_space)

        next_state, reward, done, all_reward = env.step(action)  # Step
        if reward > 0:
            rrrr = 5
        counter += 1
        if args.reward_clip > 0:
            # Clip rewards
            reward = max(min(reward, args.reward_clip), -args.reward_clip)
        tempmem.append([state, action, reward, done])  # Append transition to memory
        epi_reward += reward
        mov_q = mov_q * (1 - alpha) + alpha * Q
        # lr = trainer.dqn.get_lr()
        # mov_l = trainer.mov_l
        tr.set_postfix(
            act=actiontext(action),
            R=f"{mov_r:0.2f}",
            L=f"{mov_leng:0.1f}",
            q=f"{mov_q:0.4f}",
            ml=f"{mov_l:0.4f}",
            Q=f"{Q:0.2f}",
            ts=f"{ts}",
            rf=f"{rep_freq:0.2f}",
            w=f"{ave_weight:0.8f}",
        )

        # Train and test
        if T >= args.learn_start:
            if T % args.evaluation_interval == 0:
                # If memory path provided, save it
                if args.memory is not None:
                    save_memory_flag = True

            # Checkpoint the network
            if (args.checkpoint_interval != 0) and (T % args.checkpoint_interval == 0):
                dqn_ac.save(results_dir, "checkpoint.pth")

        state = next_state
        if args.render:
            env.render()

env.close()
